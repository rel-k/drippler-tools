#!/usr/bin/python
'''
import os
os.chdir(os.path.join(os.getenv('userprofile'),'Desktop'))
'''

from __future__ import division
import csv
import MySQLdb
import time
import datetime
import calendar
from dateutil.relativedelta import *



CREDENTIALS_FILE_NAME = "credentials"
USE_PRODUCTION_DB = False
DB_USER_NAME = ""
DB_PASSWORD = ""

def getCredentials(CREDENTIALS_FILE_NAME):
	global USE_PRODUCTION_DB
	global DB_USER_NAME
	global DB_PASSWORD
	with open(CREDENTIALS_FILE_NAME, 'r') as f:			 		
		if f.readline().split()[0] == 'True':			
			USE_PRODUCTION_DB = True
			DB_USER_NAME = f.readline().split()[0]
			DB_PASSWORD = f.readline().split()[0]
			print "Querying DB as user {0}".format(DB_USER_NAME)
		else:
			USE_PRODUCTION_DB = False
			print "Querying local DB"
	
def runQueries(dbUserName, dbPassword):	
	if USE_PRODUCTION_DB:
		db = MySQLdb.connect(host="drippler.com", user=dbUserName, passwd=dbPassword, db="drippler_integ")
	else:
		db = MySQLdb.connect(host="192.168.2.105", user="root", passwd="", db="drippler_integ")
	
	cur = db.cursor()
	queryRunTime = datetime.datetime.now().replace(hour=7, minute=0, second=0)
	queries = buildQueries(queryRunTime);
	
	with open('monthly_query_results_'  + queryRunTime.strftime('%Y-%m-%d-%H-%M-%S') + '.csv', 'w') as csvfile:
	
		fieldnames = ['Query', 'Result']
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames, quotechar='"', quoting=csv.QUOTE_ALL)
				
		writer.writeheader()			
		# Use all the SQL you like		
		for queryIndex, query in enumerate(queries):					
			cur.execute(query)
			# print all the first cell of all the rows
			curRes = cur.fetchall()
			if len(curRes) > 1:
				print "ERROR: More than 1 cell of results!"				
			print "\tquery {0}/{1}: {2} users".format(queryIndex + 1, len(queries), curRes[0][0])		
			dic = {}
			dic[fieldnames[0]] = query
			dic[fieldnames[1]] = curRes[0][0]
			writer.writerow(dic)				
def buildQueries(queryRunTime):
	queries = [];
	prefix = 'SELECT COUNT(udid) FROM devices WHERE platform = {0}  AND'
	suffix_first_run = ' first_run >=  UNIX_TIMESTAMP(\'{1}\') {2}'
	suffix_last_run  = ' last_run  >=  UNIX_TIMESTAMP(\'{1}\') {2}'
	suffix_locale  = '  AND LCASE(locale) LIKE \'us\''	
	oneMonthAgo  = queryRunTime - relativedelta(months=1)
	oneWeekAgo  = queryRunTime - relativedelta(days=7)
	times = [oneMonthAgo, oneWeekAgo]
	platforms = ['1', '2']
	localeStrings = ['', suffix_locale]
	suffixes = [suffix_last_run, suffix_first_run]
	#Save Sql queries to file
	queryOutput = open('output_'  + queryRunTime.strftime('%Y-%m-%d-%H-%M-%S') + '.sql', 'w')	
	for time in times:
		for platform in platforms:
			for locale in localeStrings:
				for suffix in suffixes:
					queryString = (prefix + suffix + locale).format(platform, time.strftime('%Y-%m-%d %H:%M:%S'), locale)					
					queries.append(queryString)
					queryOutput.write(queryString + '\n')				
	queryOutput.close()	
	return queries;

getCredentials(CREDENTIALS_FILE_NAME)
if USE_PRODUCTION_DB and (not DB_USER_NAME or not DB_PASSWORD) :
	print "DB user name or password were invalid.\nPlease set DB_USER_NAME and DB_PASSWORD in the code and try again.\n"
else:	
	runQueries(DB_USER_NAME, DB_PASSWORD);