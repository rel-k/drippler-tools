#!/usr/bin/python
'''
import os
os.chdir(os.path.join(os.getenv('userprofile'),'Desktop'))
'''
from __future__ import division
import csv
import MySQLdb
import time
import datetime
import calendar
from dateutil.relativedelta import *


BASE_DAY_FOR_GEO_ANALYSIS_FILE_NAME = "input_time.txt"
CREDENTIALS_FILE_NAME = "credentials"
USE_PRODUCTION_DB = False
DB_USER_NAME = ""
DB_PASSWORD = ""

class LocaleClass:		
	def __init__(self, aLocaleName):	
		self.localeName = aLocaleName			
		self.localeQueries = []
		self.localeResults = []
	def addQuery(self, queryString):
		self.localeQueries.append(queryString)
	def getQueries(self):
		return self.localeQueries
	def addResult(self, result):
		self.localeResults.append(result)
	def getResultByIndex(self, index):
		return self.localeResults[index]
	def buildCSVResultDictionary(self, fieldnames):
		#print "Field names #{0}, results #{1}".format(len(fieldnames), len(self.localeResults))
		dic = {}
		dic[fieldnames[0]] = self.localeName
		for index, result in enumerate(self.localeResults):
			dic[fieldnames[index + 1]] = self.localeResults[index]
		return dic
	def buildGraphReadyResultDictionary(self, fieldnames):
		dic = {}
		dic[fieldnames[0]] = self.localeName		
		index = 0
		resultIndex = 1
		while index < len(self.localeResults):
			dic[fieldnames[resultIndex]] = round(100 * (self.localeResults[index + 1] / self.localeResults[index]), 2)
			index = index + 2
			resultIndex = resultIndex + 1
		return dic
		
def getCredentials(CREDENTIALS_FILE_NAME):
	global USE_PRODUCTION_DB
	global DB_USER_NAME
	global DB_PASSWORD
	with open(CREDENTIALS_FILE_NAME, 'r') as f:			 		
		if f.readline().split()[0] == 'True':			
			USE_PRODUCTION_DB = True
			DB_USER_NAME = f.readline().split()[0]
			DB_PASSWORD = f.readline().split()[0]
			print "Querying DB as user {0}".format(DB_USER_NAME)
		else:
			USE_PRODUCTION_DB = False
			print "Querying local DB"

def buildTimePeriods(inputFileName):
	f = open(inputFileName, 'r')
	timeStringNoHours = f.read()
	timeString = timeStringNoHours + " 07:00:00" 
	f.close()
	#The base time, in GMT (00:00:00 in PST)
	currentTime = datetime.datetime.strptime(timeString, '%Y-%m-%d %H:%M:%S')
	startThisMonth = currentTime - relativedelta(months=1) 
	startThisWeek = currentTime - relativedelta(days=7)
	startThisDay = currentTime - relativedelta(days=1)
	
	#Get the time periods (12m,9m,6m,2m,1w):
	month_12_start = currentTime - relativedelta(months=12)
	month_12_end = month_12_start + relativedelta(months=1)
	month_9_start = currentTime - relativedelta(months=9)
	month_9_end = month_9_start + relativedelta(months=1)
	month_6_start = currentTime - relativedelta(months=6)
	month_6_end = month_6_start + relativedelta(months=1)
	month_2_start = currentTime - relativedelta(months=2)
	month_2_end = month_2_start + relativedelta(months=1)
	week_start = currentTime - relativedelta(days=7)
	week_end = week_start + relativedelta(days=1)
	#AK: unused:
	'''
	day_start = currentTime - relativedelta(days=1)
	day_end = day_start + relativedelta(days=1)
	'''	
	return (timeStringNoHours, [(month_12_start, month_12_end, startThisMonth, currentTime), (month_9_start, month_9_end, startThisMonth, currentTime), (month_6_start, month_6_end, startThisMonth, currentTime), (month_2_start, month_2_end, startThisMonth, currentTime), (week_start, week_end, startThisDay, currentTime)])

def runQueries(timeStringNoHours, localeClassList, dbUserName, dbPassword):	
	if USE_PRODUCTION_DB:
		db = MySQLdb.connect(host="drippler.com", user=dbUserName, passwd=dbPassword, db="drippler_integ")
	else:
		db = MySQLdb.connect(host="192.168.2.105", user="root", passwd="", db="drippler_integ_new")
	
	cur = db.cursor()
	
	#Save Sql query results to file
	results = open('results' + timeStringNoHours, 'w')
	#Save the results as a graph ready csv
	graphReady = open('graphReady' + timeStringNoHours + '.csv', 'w')
	
	with open('results'+ timeStringNoHours + '.csv', 'w') as csvfile:
		fieldnames = ['Locale', '12 months', '12 months (limited)', '9 months', '9 months (limited)', '6 months', '6 months (limited)', '2 months', '2 months (limited)', '1 week', '1 week (limited)']
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
		writer.writeheader()
		
		fieldnamesForGraph = ['Locale', '12 months', '9 months', '6 months', '2 months', '1 week']
		graphReadywriter = csv.DictWriter(graphReady, fieldnames=fieldnamesForGraph)
		graphReadywriter.writeheader()
		
		# Use all the SQL you like
		for index, localeClass in enumerate(localeClassList):			
			print "locale {0} ({1}/{2}):".format(localeClass.localeName, index + 1, len(localeClassList))
			for queryIndex, query in enumerate(localeClass.getQueries()):			
				cur.execute(query)
				# print all the first cell of all the rows
				curRes = cur.fetchall()
				if len(curRes) > 1:
					print "ERROR: More than 1 cell of results!"				
				print "\tquery {0}/{1}: {2} users".format(queryIndex + 1, len(localeClass.getQueries()), curRes[0][0])
				results.write("\tquery {0}/{1}: {2} users".format(queryIndex + 1, len(localeClass.getQueries()), curRes[0][0]))	
				localeClass.addResult(curRes[0][0])
			writer.writerow(localeClass.buildCSVResultDictionary(fieldnames))	
			graphReadywriter.writerow(localeClass.buildGraphReadyResultDictionary(fieldnamesForGraph))	
	results.close()
	graphReady.close()

def buildCohortQueries(times, localeClassList):
	#Save Sql queries to file
	output = open('output'  + timeStringNoHours +'.sql', 'w')	
	for locale in locales:
		currentLocale = LocaleClass(locale)
		for cohort in times:
			cohortString = cohort[0].strftime("%d/%m/%y") + ' - ' + cohort[1].strftime("%d/%m/%y")
			output.write(';' + cohortString + ':\n')
			query = 'SELECT COUNT(udid) FROM devices WHERE platform = \'1\' AND  (first_run BETWEEN UNIX_TIMESTAMP(\'{0}\') AND UNIX_TIMESTAMP(\'{1}\')) AND (locale={2})'.format(cohort[0], cohort[1], locale)
			#print query
			output.write(query + '\n')			
			currentLocale.addQuery(query)
			query = query + ' AND (last_run BETWEEN UNIX_TIMESTAMP(\'{0}\') AND UNIX_TIMESTAMP(\'{1}\'))'.format(cohort[2], cohort[3], locale)
			currentLocale.addQuery(query)
			#print query + '\n\n'
			output.write(query + '\n')
			#print '******************************\n'
		localeClassList.append(currentLocale)
	output.close()

(timeStringNoHours, times) = buildTimePeriods(BASE_DAY_FOR_GEO_ANALYSIS_FILE_NAME);
#print (times)

#Desired Locales:
locales = ['\'us\'', '\'in\'', "\'uk\' OR locale=\'gb\'", '\'ca\'', '\'de\'', '\'au\'', '\'ro\'', '\'ph\'', '\'my\'', '\'il\'', '\'kr\'','\'de\'','\'jp\'','\'in\'','\'br\'']
#for debug
#locales = [("\'uk\' OR locale=\'gb\'")]
#locales = ['\'kr\'']
#locales = ['\'kr\'','\'de\'','\'jp\'','\'in\'','\'br\'']

getCredentials(CREDENTIALS_FILE_NAME)
if USE_PRODUCTION_DB and (not DB_USER_NAME or not DB_PASSWORD) :
	print "DB user name or password were invalid.\nPlease set DB_USER_NAME and DB_PASSWORD in the code and try again.\n"
else:
	#keep a list of the locale classes
	localeClassList = [];
	#Prepare the queries for the time periods and locale classes
	buildCohortQueries(times, localeClassList);
	runQueries(timeStringNoHours, localeClassList, DB_USER_NAME, DB_PASSWORD);