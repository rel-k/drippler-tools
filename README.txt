To get the script set up:
1. Download the latest version of python 2.* from https://www.python.org/downloads/ and install it
2. Add the folder where you installed python to the System path (check it out here: http://bfy.tw/1RnJ)
3. Download the dateutil library from https://labix.org/python-dateutil#head-2f49784d6b27bae60cde1cff6a535663cf87497b (make sure the version is for python < 3), extract it to a folder of your chosing
4. Open a command prompt, navigate to the folder you chose in 2, and run "python setup.py install"
5. Open https://pypi.python.org/pypi/setuptools in a web browser and download ez_setup.py, run "python ez_setup.py"
6. Download MySQL for Python at http://sourceforge.net/projects/mysql-python/files/. Run the executable downloaded
To run a series of queries:
1. edit "input queries.txt" - note that each query should be in a single line
2. in a command prompt run - python "query runner.py"
To run scripts against the production DB:
1. Open and edit the "credentials" file
2. Change the first line to True and press enter
3. Write your DB user name and press enter
4. Write your DB password and press enter
To run "cohorts.py" script:
1. change the date in input_time.txt to the date you wish to test (this will be the end date of the time periods) 
4. in a command prompt run - "python cohorts.py"
To run "Monthly status check.py" script:
1. in a command prompt run - python "Monthly status check.py"