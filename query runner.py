#!/usr/bin/python
'''
import os
os.chdir(os.path.join(os.getenv('userprofile'),'Desktop'))
'''
from __future__ import division
import csv
import MySQLdb
import time
import datetime
import calendar
import re
from dateutil.relativedelta import *


INPUT_FILE_NAME = "input queries.txt"

#USE_PRODUCTION_DB = True
USE_PRODUCTION_DB = False
#AK: TODO - Change the following to your DB credentials
DB_USER_NAME = ""
DB_PASSWORD = ""

class LocaleClass:		
	def __init__(self, aLocaleName):	
		self.localeName = aLocaleName			
		self.localeQueries = []
		self.localeResults = []
	def addQuery(self, queryString):
		self.localeQueries.append(queryString)
	def getQueries(self):
		return self.localeQueries
	def addResult(self, result):
		self.localeResults.append(result)
	def getResultByIndex(self, index):
		return self.localeResults[index]
	def buildCSVResultDictionary(self, fieldnames):
		#print "Field names #{0}, results #{1}".format(len(fieldnames), len(self.localeResults))
		dic = {}
		dic[fieldnames[0]] = self.localeName
		for index, result in enumerate(self.localeResults):
			dic[fieldnames[index + 1]] = self.localeResults[index]
		return dic
	def buildGraphReadyResultDictionary(self, fieldnames):
		dic = {}
		dic[fieldnames[0]] = self.localeName		
		index = 0
		resultIndex = 1
		while index < len(self.localeResults):
			dic[fieldnames[resultIndex]] = round(100 * (self.localeResults[index + 1] / self.localeResults[index]), 2)
			index = index + 2
			resultIndex = resultIndex + 1
		return dic
		
def getQueries(inputFileName):
	with open(inputFileName) as f:
		queries = f.readlines()
	return queries

def getFieldNames (query):
	dic = {}
	p = re.compile('select distinct? (.+) from', re.IGNORECASE)
	res = p.match(query)
	if None != res:
		return filter(None, re.split(',| ', res.group(1)))
	else:
		return None
def runQueries(queries, dbUserName, dbPassword):	
	if USE_PRODUCTION_DB:
		db = MySQLdb.connect(host="drippler.com", user=dbUserName, passwd=dbPassword, db="drippler_integ")
	else:
		db = MySQLdb.connect(host="192.168.2.105", user="root", passwd="", db="drippler_integ_new")	
	cur = db.cursor()		
	# Use all the SQL you like
	for index, query in enumerate(queries):		
		fieldnames = ['Query']
		queryFields = getFieldNames(query)		
		if None == queryFields:				
			print 'queryFields was empty, exiting'
			return
		print 'query: {0}'.format(query)
		print 'After parsing the sql query len(queryFields): {0}, queryFields: {1}'.format(len(queryFields), queryFields)
		with open('query_{0}_results.csv'.format(index), 'w') as csvfile:			
			fieldnames = ['Query'] + queryFields
			writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
			writer.writeheader()
			dic = {}
			cur.execute(query)			
			curRes = cur.fetchall()			
			dic['Query'] = query
			if len(curRes) > 0 and len(curRes[0]) != len(queryFields):
					print "ERROR: invalid number of columns in result!"
			for resultRowIndex, resultRow in enumerate(curRes):				
				#print resultRow
				#print 'resultRow: {0}'.format(resultRow)
				for fieldNameIndex, fieldName in enumerate(queryFields):													
					#print 'dic: {0}, len(queryFields): {1}, len(resultRow): {2}'.format(dic, len(queryFields), len(resultRow))
					#print 'len(queryFields): {0}, len(resultRow): {1}'.format(len(queryFields), len(resultRow))
					#print 'fieldName: {0}, fieldNameIndex: {1}'.format(fieldName, fieldNameIndex)
					dic[fieldName] = resultRow[fieldNameIndex]				
				if resultRowIndex != 0:
					dic['Query'] = '{0}'.format(index)
				writer.writerow(dic)

if USE_PRODUCTION_DB and (not DB_USER_NAME or not DB_PASSWORD) :
	print "DB user name or password were invalid.\nPlease set DB_USER_NAME and DB_PASSWORD in the code and try again.\n"
else:	
	runQueries(getQueries(INPUT_FILE_NAME), DB_USER_NAME, DB_PASSWORD)